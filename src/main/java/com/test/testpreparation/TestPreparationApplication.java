package com.test.testpreparation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class TestPreparationApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestPreparationApplication.class, args);
	}

}
