package com.test.testpreparation.controllers;

import com.test.testpreparation.controllers.dto.ResponseMessage;
import com.test.testpreparation.entities.Exercise;
import com.test.testpreparation.helpers.CSVHelper;
import com.test.testpreparation.services.ExerciseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@RestController

public class ExerciseController {

    private final ExerciseService exerciseService;

    public ExerciseController(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    @PostMapping("/exercises/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (CSVHelper.hasCSVFormat(file)) {
            exerciseService.createFromCSVFile(file);
            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(message));
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    @GetMapping("/exercises")
    public ResponseEntity<Set<Exercise>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(exerciseService.findAll());
    }

    @GetMapping("/exercises/{code}")
    public ResponseEntity<Exercise> findExerciseByCode(@PathVariable("code") String code) {
        return ResponseEntity.status(HttpStatus.OK).body(exerciseService.findByCode(code));
    }

    @DeleteMapping("/exercises")
    public ResponseEntity<ResponseMessage> deleteExercises() {
        exerciseService.deleteAll();
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("All exercises has been successfully deleted"));
    }
}
