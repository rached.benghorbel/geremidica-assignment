package com.test.testpreparation.entities;

import com.test.testpreparation.entities.enums.Source;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "code", unique = true, nullable = false)
    private String code;

    @Column(name = "source", nullable = false)
    @Enumerated(EnumType.STRING)
    private Source source;

    @Column(name = "code_list_source", nullable = false)
    private String codeListCode;

    @Column(name = "display_value", nullable = false)
    private String displayValue;

    @Lob
    @Column(name = "long_description")
    private String longDescription;

    @Column(name = "from_date", nullable = false)
    private Date fromDate;

    @Column(name = "to_date")
    private Date toDate;

    @Column(name = "sorting_priority")
    private Integer sortingPriority;

    public Exercise(String code, Source source, String codeListCode, String displayValue, Date fromDate) {
        this.code = code;
        this.source = source;
        this.codeListCode = codeListCode;
        this.displayValue = displayValue;
        this.fromDate = fromDate;
    }

    public Exercise() {

    }

    public UUID getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Source getSource() {
        return source;
    }

    public String getCodeListCode() {
        return codeListCode;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public Integer getSortingPriority() {
        return sortingPriority;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public void setSortingPriority(Integer sortingPriority) {
        this.sortingPriority = sortingPriority;
    }
}
