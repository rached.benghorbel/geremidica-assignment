package com.test.testpreparation.entities.enums;

public enum Source {

    ZIB("ZIB");

    private final String value;

    Source(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    // This method meant to be used when we have all the possible values for source field
    public static Source getEnumValueFromString(String value) {
        return ZIB;
    }
}
