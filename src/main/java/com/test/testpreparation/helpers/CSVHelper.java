package com.test.testpreparation.helpers;

import com.test.testpreparation.entities.Exercise;
import com.test.testpreparation.entities.enums.Source;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {
    public static String TYPE = "text/csv";
    static String[] HEADERs = {"source", "codeListCode", "code", "displayValue", "longDescription", "fromDate", "toDate", "sortingPriority"};

    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<Exercise> csvToTutorials(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            final List<Exercise> exercises = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                final Exercise exercise = new Exercise(
                        csvRecord.get("code"),
                        Source.getEnumValueFromString(csvRecord.get("source")),
                        csvRecord.get("codeListCode"),
                        csvRecord.get("displayValue"),
                        new SimpleDateFormat("dd-MM-yyyy").parse(csvRecord.get("fromDate"))
                );
                // Setting optional fields
                if (StringUtils.hasText(csvRecord.get("longDescription"))) {
                    exercise.setLongDescription(csvRecord.get("longDescription"));
                }
                if (StringUtils.hasText(csvRecord.get("toDate"))) {
                    exercise.setToDate(new SimpleDateFormat("dd-MM-yyyy").parse(csvRecord.get("toDate")));
                }
                if (StringUtils.hasText(csvRecord.get("sortingPriority"))) {
                    exercise.setSortingPriority(Integer.getInteger(csvRecord.get("sortingPriority")));
                }
                exercises.add(exercise);
            }

            return exercises;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
