package com.test.testpreparation.repositiries;

import com.test.testpreparation.entities.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, UUID> {

    Optional<Exercise> findExerciseByCode(@Param("code") String code);

    @Query(value = "Delete from exercise ", nativeQuery = true)
    @Modifying
    void forceDeleteAllData();

}
