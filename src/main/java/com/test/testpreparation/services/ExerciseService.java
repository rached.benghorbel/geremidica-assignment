package com.test.testpreparation.services;

import com.test.testpreparation.entities.Exercise;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

public interface ExerciseService {

    /**
     * @param file in csv format
     *             This method accept a csv file and fill the database with its content
     */
    void createFromCSVFile(MultipartFile file);

    Set<Exercise> findAll();

    Exercise findByCode(String code);

    void deleteAll();

}
