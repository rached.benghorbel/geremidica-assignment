package com.test.testpreparation.services;

import com.test.testpreparation.entities.TestEntity;

import java.util.Set;

public interface TestService {

    /**
     * @param testEntity to be created
     * @return created test instance
     */
    TestEntity create(TestEntity testEntity);

    /**
     * @return all tests
     */
    Set<TestEntity> getAll();
}
