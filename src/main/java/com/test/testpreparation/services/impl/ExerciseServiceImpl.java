package com.test.testpreparation.services.impl;

import com.test.testpreparation.entities.Exercise;
import com.test.testpreparation.helpers.CSVHelper;
import com.test.testpreparation.repositiries.ExerciseRepository;
import com.test.testpreparation.services.ExerciseService;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ExerciseServiceImpl implements ExerciseService {

    private final ExerciseRepository exerciseRepository;

    public ExerciseServiceImpl(ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    @Override
    public void createFromCSVFile(MultipartFile file) {
        try {
            List<Exercise> exercises = CSVHelper.csvToTutorials(file.getInputStream());
            exerciseRepository.saveAll(exercises);
        } catch (IOException e) {
            throw new IllegalArgumentException("fail to store csv data: " + e.getMessage());
        }
    }

    @Override
    public Set<Exercise> findAll() {
        return new HashSet<>(exerciseRepository.findAll());
    }

    @Override
    public Exercise findByCode(String code) {
        return exerciseRepository.findExerciseByCode(code).orElseThrow(
                () -> new EntityNotFoundException("Exercise with code "+ code +" was not found")
        );
    }

    @Override
    @Transactional
    public void deleteAll() {
        exerciseRepository.forceDeleteAllData();
    }
}
