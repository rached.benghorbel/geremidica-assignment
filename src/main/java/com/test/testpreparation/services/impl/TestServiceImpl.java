package com.test.testpreparation.services.impl;

import com.test.testpreparation.entities.TestEntity;
import com.test.testpreparation.repositiries.TestRepository;
import com.test.testpreparation.services.TestService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class TestServiceImpl implements TestService {

    private final TestRepository testRepository;

    public TestServiceImpl(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Override
    public TestEntity create(TestEntity testEntity) {
        return testRepository.saveAndFlush(testEntity);
    }

    @Override
    public Set<TestEntity> getAll() {
        return new HashSet<>(testRepository.findAll());
    }
}
