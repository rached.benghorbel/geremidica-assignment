package com.test.testpreparation.services.impl;

import com.test.testpreparation.entities.TestEntity;
import com.test.testpreparation.services.TestService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class TestServiceImplTestEntity {

    @Autowired
    private TestService testService;

    @Test
    void should_save_new_test() {
        // GIVEN
        final TestEntity firstTestEntity = buildTestEntity("first test");

        // WHEN
        final TestEntity createdTestEntity = testService.create(firstTestEntity);

        // THEN
        Assertions.assertThat(createdTestEntity.getName()).isEqualTo("first test");
        Assertions.assertThat(createdTestEntity.getId()).isNotNull();
        Assertions.assertThat(createdTestEntity.getVersion()).isEqualTo(0);

    }

    private TestEntity buildTestEntity(String name) {
        final TestEntity testEntity = new TestEntity();
        testEntity.setName(name);
        return testEntity;
    }

}